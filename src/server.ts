import express from 'express'
import { sayHello } from './say-hello'

const app = express()

app.use(express.static('public'))

app.get('/hello', (req, res) => {
  const { query: { name } } = req
  if (!name) {
    res.status(400).send('Error: a name must be provided (eg: /hello?name=tom')
    return
  }
  res.send(sayHello(name))
})

app.get('/version', (req, res) => {
  res.send('Version deployed: {{sha}}')
})

export default app
